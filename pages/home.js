import axios from "axios"
import { useEffect, useState } from "react"
import modal from "./modal"

const Home = () => {
    const [token, setToken] = useState('')
    const [siswa, setSiswa] = useState([])

    useEffect(() => {
        const storage = JSON.parse(localStorage.getItem('user'))
        console.log(storage.users_token)
        setToken(storage.users_token)

        axios.get(modal.siswa, {
            headers: {
                'Authorization' : `Bearer ${token}`,
                'Content-type' : 'application/json',
            }
        }).then((res) => {
            setSiswa(res.data)
            console.log(res.data)
        })
    }, [])

    return (
        <div>
            <h3>Berhasil login</h3>
        </div>
    )
}

export default Home