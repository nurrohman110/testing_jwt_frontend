import axios from "axios";
import Router from "next/router";
import { useState } from "react";
import modal from "./modal";

const Login = () => {
	const [handleChange, setHandleChange] = useState()

	const login = (data) => {
		const test = {
			'email' : data.email,
			'password' : data.password,
		};
		axios.post(modal.login, test).then((res) => {
			console.log(res.data)
			localStorage.setItem("user", JSON.stringify(res.data.data));
			if(res.status == 200){
				Router.push('/home')
			}
		});
	}
	const handleChangeInput = e => {
        const {name, value} = e.target
        setHandleChange({...handleChange, [name] : value})
    }
	
    return (
		<div class="container">
			<div class="row">
				<div class="span12">
					<form class="form-horizontal" onSubmit={(e) => {
						e.preventDefault()
						login(handleChange)
					}} method="POST">
					<fieldset>
						<div id="legend">
						<legend class="">Login</legend>
						</div>
						<div class="control-group">
						<label class="control-label"  for="username">Email</label>
						<div class="controls">
							<input type="text" id="email" name="email" onChange={handleChangeInput} placeholder="" class="input-xlarge" />
						</div>
						</div>
						<div class="control-group">
						<label class="control-label" for="password">Password</label>
						<div class="controls">
							<input type="password" id="password" onChange={handleChangeInput} name="password" placeholder="" class="input-xlarge" />
						</div>
						</div>
						<div class="control-group">
						<div class="controls">
							<button class="btn btn-success">Login</button>
						</div>
						</div>
					</fieldset>
					</form>
				</div>
			</div>
		</div>
    )
}
export default Login